#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <set>
#include <stdexcept>
#include <assert.h>     /* assert */

/////////////////////////////////////////////////////
// Class interface---
// class giving access to data is derived from this
typedef float Key_type;

class InputArray {
private:
	mutable unsigned m_numo_queries;

protected:
	virtual Key_type access_item(unsigned j) const = 0;

public:
	virtual unsigned size() const = 0;

	Key_type operator[](unsigned j) const          { ++m_numo_queries; return access_item(j); }

	const unsigned numo_queries() { return m_numo_queries; }

	InputArray() : m_numo_queries(0) {}
};

/////////////////////////////////////////////////////
// Some stupid implementation for testing
class SimpleTest_InputArray : public InputArray {
	const unsigned            n;
	std::set<unsigned>        damaged;

protected:
	virtual Key_type access_item(unsigned j) const { return damaged.count(j) ? -1. : std::sqrt(j); }
	virtual unsigned size() const                  { return n; }

public:
	SimpleTest_InputArray(unsigned _n = 1000000000, double delta = .00001) : n(_n) {
		if (n <= 1)            throw std::runtime_error("SimpleTest_InputArray constructor: n too small");
		if (n >= RAND_MAX)     throw std::runtime_error("SimpleTest_InputArray constructor: n too big");
		if (delta*n >= n / 3)  throw std::runtime_error("SimpleTest_InputArray constructor: delta too big");
		if (delta*n <= 2.0)  throw std::runtime_error("SimpleTest_InputArray constructor: n too small (compared to delta)");
		// flip a coin
		if (std::rand() % 2) { // "Heads"
			for (unsigned i = 0; i<delta*n; ++i) {
				unsigned e;
				do {
					e = n / 3 + std::rand() % (2 * n / 3);
				} while (damaged.count(e));
				damaged.insert(e);
			}
		} // if "Heads"
	}
};


/////////////////////////////////////////////////////
// Example implementation: trivial test w/ n queries
bool trivial__sorted_test(const InputArray & x, double epsilon)
{
	// This doesn't need the epsilon
	const unsigned n = x.size();
	if (n == 0) return true;
	Key_type x_prev = x[0];
	for (unsigned j = 1; j<n; ++j) {
		Key_type x_curr = x[j];
		if (x_prev > x_curr) return false;
		x_prev = x_curr;
	}
	return true;
} // trivial__sorted_test()



//===================================================
/////////////////////////////////////////////////////
// I m p l e m e n t   t h i s   f u n c t i o n :
//
bool sublinear__sorted_test(const InputArray & x, double epsilon)
{
	// Construct a graph(2 - spanner) <= n log n edges
	// Sample (4 log n)/epsilon 2 edges (xi ,xj) from the 2 - spanner and reject if xi > xj

	const unsigned n = x.size();
	if (n == 0) return true;

	int q = 2 * log10((double)n) / (epsilon);
	int i = 0, j, k = 0;
	do {
		i = std::rand() % (n-1);
		j = std::rand() % (n - i) + i;
		if(i > j) throw std::runtime_error("fck");
		Key_type xi = x[i];
		Key_type xj = x[j];
		if (xi > xj) return false;
		k++;
	} while (k < q);

	return true;
}

//===================================================
/////////////////////////////////////////////////////
// I m p l e m e n t   t h i s   f u n c t i o n :
//
bool sublinear__sorted_test_alternative(const InputArray & x, double epsilon)
{
	// Construct a graph(2 - spanner) <= n log n edges
	// Sample (4 log n)/epsilon 2 edges (xi ,xj) from the 2 - spanner and reject if xi > xj

	const unsigned n = x.size();
	if (n == 0) return true;

	int q = 4 * log10(n) / (epsilon);
	Key_type x_prev = x[0];
	int i = 0, j, k = 0;
	do {
		j = std::rand() % n;
		if (i == j) continue;
		Key_type x_curr = x[j];
		if (i< j && x_prev > x_curr) return false;
		if (i> j && x_prev < x_curr) return false;
		x_prev = x_curr;
		i = j;
		k++;
	} while (k < q);

	return true;
}

/////////////////////////////////////////////////////
int main() {
	std::srand(std::time(0) % 65537);
#ifdef WIN32 
	// RAND_MAX = 0x7fff
	const unsigned n = 32000;   // number of keys in the array
#else
	const unsigned n = 1000000000;   // number of keys in the array
#endif
	const double   delta = .001;         // "damage" to sorted structure
	const double   epsilon = .001;         // epsilon for the algorithm (lower bound on "damage")
	std::cout << "Creating the array object..." << std::flush;
	SimpleTest_InputArray x(n, delta);

	std::cout << "done\nRunning your sublinear test..." << std::flush;
	const std::clock_t t_0 = std::clock();
	//const bool is_sorted = trivial__sorted_test(x, epsilon);
	const bool is_sorted = sublinear__sorted_test(x,epsilon);
	//const bool is_sorted = sublinear__sorted_test_alternative(x, epsilon);
	const std::clock_t t_1 = std::clock();
	std::cout << "done" << std::endl;

	if (is_sorted) {
		std::cout << "Array is probably sorted." << std::endl;
	}
	else {
		std::cout << "Array is definitely not sorted." << std::endl;
	}
	std::cout << "It required " << x.numo_queries() << " queries and took " << double(t_1 - t_0) / CLOCKS_PER_SEC * 1000 << " millisec." << std::endl;
	std::cin.get();
} // main()
